"use strict";

import Color from './color.mjs';
import Coordinates from './coordinates.mjs';
import MoveType from './move_type.mjs';
import OpenXum from '../../openxum/index.mjs';

class Move extends OpenXum.Move {
  constructor(t, c, c1, c2) {
    super();
    this._type = t;
    this._color = c;
    if (this._type === MoveType.PUT_RING || this._type === MoveType.PUT_MARKER || this._type === MoveType.REMOVE_RING) {
      this._coordinates = c1;
    } else if (this._type === MoveType.MOVE_RING) {
      this._from = c1;
      this._to = c2;
    } else {
      this._row = c1;
    }
  }

// public methods
  color() {
    return this._color;
  }

  coordinates() {
    return this._coordinates;
  }

  decode(str) {
    const type = str.substring(0, 2);

    if (type === 'Pr') {
      this._type = MoveType.PUT_RING;
    } else if (type === 'Pm') {
      this._type = MoveType.PUT_MARKER;
    } else if (type === 'Rr') {
      this._type = MoveType.REMOVE_RING;
    } else if (type === 'Mr') {
      this._type = MoveType.MOVE_RING;
    } else if (type === 'RR') {
      this._type = MoveType.REMOVE_ROW;
    }
    this._color = str.charAt(2) === 'B' ? Color.BLACK : Color.WHITE;
    if (this._type === MoveType.PUT_RING || this._type === MoveType.PUT_MARKER || this._type === MoveType.REMOVE_RING) {
      this._coordinates = new Coordinates(str.charAt(3), parseInt(str.charAt(4)));
    } else if (this._type === MoveType.MOVE_RING) {
      this._from = new Coordinates(str.charAt(3), parseInt(str.charAt(4)));
      this._to = new Coordinates(str.charAt(5), parseInt(str.charAt(6)));
    } else {
      this._row = [];
      for (let index = 0; index < 5; ++index) {
        this._row.push(new Coordinates(str.charAt(3 + 2 * index),
          parseInt(str.charAt(4 + 2 * index))));
      }
    }
  }

  encode() {
    if (this._type === MoveType.PUT_RING) {
      return 'Pr' + (this._color === Color.BLACK ? "B" : "W") + this._coordinates.to_string();
    } else if (this._type === MoveType.PUT_MARKER) {
      return 'Pm' + (this._color === Color.BLACK ? "B" : "W") + this._coordinates.to_string();
    } else if (this._type === MoveType.REMOVE_RING) {
      return 'Rr' + (this._color === Color.BLACK ? "B" : "W") + this._coordinates.to_string();
    } else if (this._type === MoveType.MOVE_RING) {
      return 'Mr' + (this._color === Color.BLACK ? "B" : "W") + this._from.to_string() + this._to.to_string();
    } else {
      var str = 'RR' + (this._color === Color.BLACK ? "B" : "W");

      for (let index = 0; index < this._row.length; ++index) {
        str += this._row[index].to_string();
      }
      return str;
    }
  }

  from() {
    return this._from;
  }

  row() {
    return this._row;
  }

  to() {
    return this._to;
  }

  to_object() {
    return {
      type: this._type,
      color: this._color,
      coordinates: this._coordinates,
      from: this._from,
      to: this._to,
      row: this._row
    };
  }

  to_string() {
    if (this._type === MoveType.PUT_RING) {
      return 'put ' + (this._color === Color.BLACK ? 'black' : 'white') + ' ring at ' + this._coordinates.to_string();
    } else if (this._type === MoveType.PUT_MARKER) {
      return 'put ' + (this._color === Color.BLACK ? 'black' : 'white') + ' marker at ' + this._coordinates.to_string();
    } else if (this._type === MoveType.REMOVE_RING) {
      return 'remove ' + (this._color === Color.BLACK ? 'black' : 'white') + ' ring at ' + this._coordinates.to_string();
    } else if (this._type === MoveType.MOVE_RING) {
      return 'move ' + (this._color === Color.BLACK ? 'black' : 'white') + ' ring from ' + this._from.to_string() + ' to ' + this._to.to_string();
    } else {
      let str = 'remove ' + (this._color === Color.BLACK ? 'black' : 'white') + ' row ( ';

      for (let index = 0; index < this._row.length; ++index) {
        str += this._row[index].to_string() + ' ';
      }
      str += ')';
      return str;
    }
  }

  type() {
    return this._type;
  }
}

export default Move;